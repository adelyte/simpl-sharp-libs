require 'nokogiri'
require 'httparty'
require 'json'

ROOT = "http://www.nivloc.com"

def download_nunit(latest)
  html = Nokogiri::HTML(HTTParty.get("#{ROOT}#{latest}NUnit"))

  date_format = "%m/%d/%Y %l:%M %p"

  latest_link = html.css('body > pre').children.select { |el|
    el.content.match('<dir>')
  }
  .map.with_index { |e, idx|
    [e.content.sub('<dir>', '').strip,e.next_sibling]
  }
  .sort { |a, b|
    DateTime.strptime(a[0], date_format) <=> DateTime.strptime(b[0], date_format)
  }
  .last

  latest_path = latest_link[1][:href]

  download_dlls latest_path
end

def download_dlls(latest)
  html = Nokogiri::HTML(HTTParty.get("#{ROOT}#{latest}"))

  dll_links = html.css("a[href$='dll']")

  dll_links.each do |link|
    dll = HTTParty.get(ROOT + link[:href])
    name = link.content.to_s

    File.open("./DLLs/#{name}", "wb") do |file|
      file.write(dll.body)
    end
  end
end


def get_latest_version
  html = Nokogiri::HTML(HTTParty.get("#{ROOT}/downloads/Crestron/SSharp"))

  date_format = "%m/%d/%Y %l:%M %p"

  latest_link = html.css('body > pre').children.select { |el|
    el.content.match('<dir>')
  }
  .map.with_index { |e, idx|
    [e.content.sub('<dir>', '').strip,e.next_sibling]
  }
  .sort { |a, b|
    DateTime.strptime(a[0], date_format) <=> DateTime.strptime(b[0], date_format)
  }
  .last

  latest_path = latest_link[1][:href]

  download_dlls "#{latest_path}DLLs"
  download_nunit latest_path
end

def main
  puts "Downloading latest version of all DLLs..."
  get_latest_version
  puts "All downloads have finished. Please check DLLs/"
end

main
