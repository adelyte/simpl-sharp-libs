# SIMPL# Libraries

To get the latest versions of all DLLs run the following commands:

0. `bundle install`
0. `ruby scripts/update.rb`
0. That's it! DLLs are stored in `./DLLs`

To use the references in Visual Studio

0. Open a project/solution
1. Right-click on "References" then "Add Reference..."
2. Navigate to the `./DLLs` directory
3. Select required DLLs (you can select multiple in one import)